﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.Models
{
    public class Product
    {
        public int ProductId { get; set; }

        public int Price { get; set; }

        public string ProdName { get; set; }

        public int Qty_In_Stock { get; set; }

        


    }
}
