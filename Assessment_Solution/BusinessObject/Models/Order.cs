﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessObject.Models
{
    public class Order
    {
        public int OrderId { get; set; }
        public DateTime DatePlaced { get; set; }

        public Product product { get; set; }//foreign key

        public User user { get; set; } //foreign key 
    }
}
