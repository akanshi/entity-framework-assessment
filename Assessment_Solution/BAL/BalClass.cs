﻿using DAL;
using BusinessObject.Models;


namespace BAL
{
    public class BalClass
    {
        DalClass dal = new DalClass();

        public int AddUser(User user)
        {
           return dal.AddUser(user);
            
        }

        public int DeleteUser(int id)
        {
            return dal.DeleteUser(id);
        }

        public int AddProduct(Product product)
        {
            return dal.AddProduct(product);
        }

        public int DeleteProduct(int id)
        {
            return dal.DeleteProduct(id);
        }

        public int UpdateProduct(string name,Product product)
        {
            return dal.UpdateProduct(name,product);

        }

        public List<Product> GetProducts()
        {
            return dal.GetProducts();
        }

        public int PlaceOrder(Order order)
        {
            return dal.PlaceOrder(order);
        }

        public List<User> GetUsers()
        {
            return dal.GetUsers();
        }
    }
}