﻿using BAL;
using BusinessObject.Models;

namespace PL
{
    internal class Program
    {
        static void Main(string[] args)
        {
            char ch = 'y';
            int option;
            BalClass bal = new BalClass();
            

            while (ch == 'y')
            {
                Console.WriteLine("  1.Admin");
                Console.WriteLine("  2.Customer");
                Console.WriteLine("  3.Supplier");

                option = Convert.ToInt32(Console.ReadLine());

                switch (option)
                {
                    case 1:

                        Console.WriteLine("  1.Add user");
                        Console.WriteLine("  2.Delete user");
                        int type = Convert.ToInt32(Console.ReadLine());

                        if(type == 1)
                        {
                            Console.WriteLine("Enter first name:");
                            string firstName = Console.ReadLine();
                            Console.WriteLine("Enter Last name:");
                            string lastName = Console.ReadLine();
                            User user = new User()
                            {
                                FirstName = firstName,
                                LastName = lastName,
                               
                                
                            };
                            bal.AddUser(user);
                        }
                        else
                        {
                            List<User> list = bal.GetUsers();
                            foreach (User user in list)
                            {
                                Console.WriteLine(user.UserId + " " + user.FirstName + " " + user.LastName);
                            }
                            Console.WriteLine("enter user id");
                            int id = Convert.ToInt32(Console.ReadLine());
                            bal.DeleteUser(id);
                        }

                        break;

                    case 2:
                        Console.WriteLine("  1.Get Products");
                        Console.WriteLine("  2.Place order");
                         type = Convert.ToInt32(Console.ReadLine());

                        if (type == 1)
                        {

                            List<Product> list = bal.GetProducts();
                            foreach(Product prod in list)
                            {
                                Console.WriteLine(prod.ProdName + " " + prod.Qty_In_Stock + " " + prod.Price);
                            }
                        }
                        else
                        {

                            Order order = new Order()
                            {
                                DatePlaced = DateTime.Now
                            };
                            bal.PlaceOrder(order);
                        }


                        break;

                    case 3:
                        Console.WriteLine("  1.Add product");
                        Console.WriteLine("  2.Delete product");
                        Console.WriteLine("  3.Update product");
                         type = Convert.ToInt32(Console.ReadLine());

                        if (type == 1)
                        {
                            Console.WriteLine("What is the price of the product?");
                            int price = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("What is the product name?");
                            string prodName = Console.ReadLine();

                            Console.WriteLine("What is the qty in stock?");
                            int qty = Convert.ToInt32(Console.ReadLine());
                            Product product = new Product()
                            {
                                Price = price,
                                ProdName = prodName,
                                Qty_In_Stock = qty
                            };
                            bal.AddProduct(product);
                        }
                        else if(type == 2)
                        {
                            List<Product> list = bal.GetProducts();
                            foreach(Product product in list)
                            {
                                Console.WriteLine(product.ProductId + " " + product.ProdName + " " + product.Price + " " + product.Qty_In_Stock);
                            }
                            Console.WriteLine("enter product id");
                            int id = Convert.ToInt32(Console.ReadLine());
                            bal.DeleteProduct(id);
                        }
                        else
                        {
                            List<Product> list = bal.GetProducts();
                            foreach(Product prod in list)
                            {
                                Console.WriteLine(prod.ProductId + " " + 
                                    prod.ProdName + " " + prod.Price + " " +prod.Qty_In_Stock );
                            }


                            Console.WriteLine("Enter the name of the product to edit");
                            string name = Console.ReadLine();
                            
                            
                                Console.WriteLine("Enter the new name");
                                string? ProductName = Console.ReadLine();
                                Console.WriteLine("Enter the new price");
                                
                                int Price;
                                Int32.TryParse(Console.ReadLine(), out Price);
                                Console.WriteLine("Enter the new Quantity");
                                int Quantity;
                                Int32.TryParse(Console.ReadLine(), out Quantity);
                                Product upDatedProduct = new Product()
                                {
                                    ProdName = ProductName,
                                    Price = Price,
                                    Qty_In_Stock = Quantity
                                };
                                int res = bal.UpdateProduct(name, upDatedProduct);
                                if(res == 0)
                            {
                                Console.WriteLine("Product has been updated!");
                            }
                            else
                            {
                                Console.WriteLine("It could not be updated");
                            }
                            

                        }

                        break;

                    default:
                        Console.WriteLine("Enter a valid choice");
                        break;

                }

                Console.WriteLine("Do you want to repeat?");
                ch = Convert.ToChar(Console.ReadLine());
            }
        }
    }
}