﻿using BusinessObject;
using BusinessObject.Models;
using DAL.Context;
using System.Net.Http.Headers;

namespace DAL
{
   public class DalClass
    {

        DBContext db = new DBContext();
        //for admin
        public int AddUser(User user) {
            List<Role> roles = db.Roles.ToList();
            foreach(Role role in roles)
            {
                Console.WriteLine(role.Id + " " + role.RoleName);
            }
            
            Console.WriteLine("Choose role id:");
            int RoleId = Convert.ToInt32(Console.ReadLine());

            Role obj = db.Roles.Find(RoleId);
            user.role = obj;
            user.role.Id = RoleId;
            db.Users.Add(user);
            db.SaveChanges();
            Console.WriteLine("User Added!");
            return 0;
        }

        public int DeleteUser(int id) {
            User obj = db.Users.Where(x => x.UserId == id).FirstOrDefault();
            if (obj != null)
            {
                obj.IsActive = true;
                Console.WriteLine("User deleted!");
                db.SaveChanges();
                return 0;
            }
            else
            {
                Console.WriteLine("No such user exists with this id");
                return 1;
            }

        }

        //for supplier
        public int AddProduct(Product product) {
            db.Products.Add(product);
            db.SaveChanges();
            Console.WriteLine("Product added!");
            return 0;
        }

        public int DeleteProduct(int id) {
           
            Product obj = db.Products.Where(x=>x.ProductId == id).FirstOrDefault();
            if(obj != null)
            {
                db.Products.Remove(obj);
                db.SaveChanges();
                Console.WriteLine("Product deleted");
                return 0;
            }
            else
            {
                Console.WriteLine("No such product exists");
                return 1;
            }


        
        }

        //public int UpdateProductPrice(int id, int price)
        //{
        //    Product prodObj = db.Products.Find(id);
        //    prodObj.Price = price;


        //    db.SaveChanges();
        //    Console.WriteLine("product updated");
        //    return 0;
        //}

        //public int UpdateProductName(int id, string name)
        //{
        //    Product prodObj = db.Products.Find(id);
        //    prodObj.ProdName = name;


        //    db.SaveChanges();
        //    Console.WriteLine("product updated");
        //    return 0;
        //}

        //public int UpdateProductQty(int id, int qty)
        //{
        //    Product prodObj = db.Products.Find(id);
        //    prodObj.Qty_In_Stock = qty;


        //    db.SaveChanges();
        //    Console.WriteLine("product updated");
        //    return 0;
        //}

        public int UpdateProduct(string name, Product product)
        {
            Product obj = db.Products.Where(x => x.ProdName == name).FirstOrDefault();
            if (obj != null)
            {
                if (product.ProdName.Length > 0)
                    obj.ProdName = product.ProdName;
                if (product.Price > 0)
                    obj.Price = product.Price;

                if (product.Qty_In_Stock > 0)
                    obj.Qty_In_Stock = product.Qty_In_Stock;

                db.Products.Update(obj);
                db.SaveChanges();
                return 0;
            }
            else
            {
                return 1;
            }


        }


        //for customer

        public List<Product> GetProducts() {
            List<Product> list = db.Products.ToList();

            return list;
        }

        public int PlaceOrder(Order order)
        {
            List<Product> products = db.Products.ToList();
            foreach(Product product in products)
            {
                Console.WriteLine(product.ProductId + " " + product.ProdName + " " + product.Price + " " +
                    product.Qty_In_Stock);
            }
            Console.WriteLine("select product id");
            int prodId = Convert.ToInt32(Console.ReadLine());
            List<User> users = db.Users.Where(x=>x.role.Id == 2).ToList();
            foreach(User usr in users)
            {
                Console.WriteLine(usr.UserId + " " + usr.FirstName + " " +
                    usr.LastName);
            }
            Console.WriteLine("select user id");
            int userId = Convert.ToInt32(Console.ReadLine());

            Product ProductObj = db.Products.Find(prodId);
            User UserObj = db.Users.Find(userId);

            order.product = ProductObj;
            order.user = UserObj;

            db.Orders.Add(order);
            db.SaveChanges();
            Console.WriteLine("Order has been placed!");
            return 0;

        }

        public List<User> GetUsers()
        {
            List<User> list = db.Users.ToList();
            return list;
        }



        





    }
}